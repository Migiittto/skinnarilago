# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.cache import cache_page
from django.core.cache import cache

from game.models import *
from game.serializers import *
import requests
import json
import collections
import jsonpickle
import datetime
import geopy.distance
# Create your views here.

def gamepage(request):
        if request.session.session_key is None or request.session.get("team", None) is None:
                return HttpResponseRedirect(reverse("teams"))
        locs = Location.objects.all()
        teams = TeamSerializer(Team.objects.all(), many=True).data
        teams = sorted(teams, key=lambda x: x["points"], reverse=True)
        context={
                "locations":json.dumps(LocationSerializer(locs, many=True).data),
                "teams":json.dumps(teams)
        }
        return render(request, 'game.html', context)

def gamestate(request):
        if request.method == "GET":
                gs = cache.get("gamestate")
                if gs is None:
                        teams = TeamSerializer(Team.objects.all(), many=True).data
                        locs = Location.objects.all()
                        teams = sorted(teams, key=lambda x: x["points"], reverse=True)
                        for t in teams:
                                t["points"]=0
                        cache.set("gamestate", json.dumps({"locs":LocationSerializer(locs, many=True).data, "teams":teams}), 3600)
                        return HttpResponse(
                                        json.dumps({"locs":LocationSerializer(locs, many=True).data, "teams":teams})
                                        )
                else:
                        return HttpResponse(gs)
        if request.method == "POST":
                js = json.loads(request.body)
                tag = js.get("tag")
                if tag == "UPDATE_LOC":
                        lochistory =request.session.get("lochistory", None)
                        if lochistory is None:
                                lochistory = collections.deque([], maxlen=10)
                        else:
                                lochistory = jsonpickle.decode(lochistory)
                        lochistory.append({"lng":js.get("lng"), "lat":js.get("lat"), "timestamp":datetime.datetime.now()})
                        request.session["lochistory"]=jsonpickle.encode(lochistory)
                        return HttpResponse("")
                if tag == "CAPTURE_LOC":
                        token = js.get("token", None)
                        if not token or not recaptcha_verify(token, request):
                                print("no token")
                                return HttpResponse("FAILURE")

                        lochistory =request.session.get("lochistory", None)
                        if lochistory is None:
                                return HttpResponse("FAILURE")
                        else:
                                lochistory = jsonpickle.decode(lochistory)
                        locationid = js.get("locid")
                        #Anti cheat block
                        for i, k in enumerate(lochistory):
                                if i>0:
                                        coords1 = (lochistory[i-1]["lng"], lochistory[i-1]["lat"])
                                        coords2 = (lochistory[i]["lng"], lochistory[i]["lat"])
                                        time1 = lochistory[i-1]["timestamp"]
                                        time2 = lochistory[i]["timestamp"]
                                        dt = time2-time1
                                        if coords1 != coords2 and dt.total_seconds() > 1:
                                                speed =  geopy.distance.vincenty(coords1, coords2).km*1000 / dt.total_seconds()
                                                if speed > 20:
                                                        return HttpResponse("FAILURE")
                        #Update location block
                        loc = Location.objects.get(id=locationid)
                        team = Team.objects.get(id=request.session["team"])
                        coords1 = (loc.longitude, loc.latitude)
                        coords2 = (js.get("lng"), js.get("lat"))
                        if geopy.distance.vincenty(coords1, coords2).km*1000<25:
                                loc.current_owner=team
                                loc.last_capture=timezone.now()
                                loc.save()
                        teams = TeamSerializer(Team.objects.all(), many=True).data
                        locs = Location.objects.all()
                        teams = sorted(teams, key=lambda x: x["points"], reverse=True)
                        for t in teams:
                                t["points"]=0
                        cache.set("gamestate", json.dumps({"locs":LocationSerializer(locs, many=True).data, "teams":teams}), 3600)
                        return HttpResponse("OK")
                return HttpResponse("NoTAG")

def get_client_ip(request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
                ip = x_forwarded_for.split(',')[0]
        else:
                ip = request.META.get('REMOTE_ADDR')
        return ip

def recaptcha_verify(token, request):
        captcha_rs = token
        url = "https://www.google.com/recaptcha/api/siteverify"
        params = {
                'secret': '6LdSOE0UAAAAAER6f-6FLTlED0TX3dokkmhL75ui',
                'response': captcha_rs,
                'remoteip': get_client_ip(request)
        }
        verify_rs = requests.get(url, params=params, verify=True)
        verify_rs = verify_rs.json()
        return verify_rs.get("success", False)

def csrf_failure(request, reason):
        return HttpResponseForbidden()

def teams(request):
        if request.method == "GET":
                teams = Team.objects.all()
                context={
                        "team":teams,
                        "teams":json.dumps(TeamSerializer(Team.objects.all(), many=True).data)
                }
                return render(request, 'team.html', context)
        if request.method == "POST":
                try:
                        request.session["team"] = request.POST.get("team")
                        return HttpResponseRedirect(reverse("landing"))
                except Exception as e:
                        print(e)
                        teams = Team.objects.all()
                        context={
                                "team":teams
                        }
                        return render(request, 'team.html', context)
