# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

#Model to hold team name and city
class Team(models.Model):
	name = models.CharField(max_length=100, default='Team Name')
	city = models.CharField(max_length=100, default='Lappeenranta')
	logo = models.ImageField(upload_to='team_image/')

	def __unicode__(self):
		return unicode(self.name+" "+self.city+" Points:"+str(self.get_points()))

	def get_points(self):
		return Point.objects.filter(team=self).count()

#The model for each capturable location
class Location(models.Model):
	name = models.CharField(max_length=100, default='Location Name')
	latitude = models.FloatField(null=False)
	longitude = models.FloatField(null=False)
	last_capture = models.DateTimeField(default=None)
	current_owner = models.ForeignKey(Team, default=None, blank=True, null=True)

	def __unicode__(self):
		try:
			return unicode(self.name +" "+ self.current_owner.name)
		except AttributeError:
			return unicode(self.name +" No owner")

#Point gets generated every X minutes.
class Point(models.Model):
	team = models.ForeignKey(Team, db_index=True)
	location = models.ForeignKey(Location, db_index=True)
	timestamp = models.DateTimeField(auto_now_add=True)
