from __future__ import absolute_import

from celery import Celery
from celery import shared_task
app = Celery("skinnarilago")
app.config_from_object('django.conf:settings')

from celery.task.schedules import crontab
from celery.decorators import periodic_task
from game.models import *
from game.serializers import *
import datetime
from django.core.cache import cache
import time
import json

@periodic_task(run_every = crontab(minute='*/5'), name="points", ignore_result=True)
def generatePoints():
    for loc in Location.objects.all():
        if loc.current_owner is not None:
            Point.objects.create(location=loc, team=loc.current_owner)
    teams = TeamSerializer(Team.objects.all(), many=True).data
    locs = Location.objects.all()
    teams = sorted(teams, key=lambda x: x["points"], reverse=True)
    for t in teams:
        t["points"]=0
    cache.set("gamestate", json.dumps({"locs":LocationSerializer(locs, many=True).data, "teams":teams}), 3600)
