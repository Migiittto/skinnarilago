"use strict";

let app = angular.module('gameApp', ["leaflet-directive", "ngAnimate", "ngSimpleToast"])

app.service("GameStateService", function($http) {
    return {
        getData: function() {
          return $http.get(GAME_STATE_URL).then(function(response) {
              return response.data;
            }, function(error) {
                console.log("Error reading gamestate data: ", error);
            });
        },
        updateLocation: function(lat, lng) {
          return $http.post(GAME_STATE_URL,
            {"lat":lat, "lng":lng, "tag":"UPDATE_LOC"}
          ).then(function(response) {
              return response.data;
            }, function(error) {
                console.log("Error reading gamestate data: ", error);
            });
        },
        captureLocation: function(lat, lng, id, token) {
          return $http.post(GAME_STATE_URL,
                            {"lat":lat, "lng":lng, "tag":"CAPTURE_LOC", "locid":id, token:token}
          ).then(function(response) {
              return response.data;
            }, function(error) {
                console.log("Error reading gamestate data: ", error);
            });
        }
    };
});

app.controller('GameController', ["$scope", "$interval", "leafletMapEvents", "GameStateService", "toast", function($scope, $interval, leafletMapEvents, GameStateService, toast){
    $scope.distFromMarker = 999;
    $scope.selectedMarker = null;
    $scope.cameraLock=true;
    $scope.teamid=TEAM_ID;

    $scope.startTracking = function(){
      if (navigator.geolocation) {
          navigator.geolocation.watchPosition(function(position, error){
            $scope.$apply(function(){
              if(error){
                $scope.showError(error);
              }
              $scope.updatePlayerLoc(position);
            })
          });
      } else {
          $scope.showError({code:"", message:"Geolocation is not supported by this browser."});
      }
    }

    $scope.startTracking();

    $scope.updatePlayerLoc = function(position){
      $scope.playerLoc = {lng:position.coords.longitude, lat:position.coords.latitude};
      if($scope.cameraLock){
        $scope.location = {
          lat:position.coords.latitude,
          lng:position.coords.longitude,
          zoom:$scope.location.zoom
        }
      }
      if($scope.selectedMarker != null){
        $scope.checkSelectedMarkerDistance();
      }
      GameStateService.updateLocation(position.coords.latitude, position.coords.longitude);
    }

    $scope.location = {
      lat:61.0630,
      lng:28.0990,
      zoom:16
    }

    $scope.legend = {
      position: 'bottomleft',
      colors: [ '#D80027', '#91DC5A', '#006DF0', '#933EC5', '#FFDA44' ],
      labels: [ 'Cluster', 'TIK', 'TiTe', 'Otit', 'Digit' ]
    }

    $scope.playerIcon={
      iconUrl:"/static/players/"+TEAM_ID+".png",
      iconSize:[50, 50],
      iconAnchor: [25, 25],
      popupAnchor:[50, 25]
    }

    $scope.teams = TEAMS;

    $scope.events = {
      map:{
        enable:['click', 'drag'],
        logic:'emit'
        }
      }

    $scope.tiles={
      url:'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
      options:{
        attribution: '',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoibWlnaWl0dG8iLCJhIjoiY2pkdDFkeG1hMHl1ZzJ4cGNkYW90a2k2byJ9.u4IdRTY7AJfEq2IdGa3f6Q'
      }
    }

    $scope.getMarkerIcon=function(id){
      if(id==null){
        return {
          iconUrl:"/static/markers/0.png",
          iconSize:[50, 50],
          iconAnchor: [25, 50],
          popupAnchor:[0, -50]
        }
      }
      return {
        iconUrl:"/static/markers/"+id+".png",
        iconSize:[50, 50],
        iconAnchor: [25, 50],
        popupAnchor:[0, -50]
      }
    }

    $scope.markers=[];
    $scope.paths=[];
    $scope.locs = LOCATIONS;
    for (var i = 0; i < $scope.locs.length; i++) {
      try {
        $scope.markers.push({lat:$scope.locs[i].latitude, lng:$scope.locs[i].longitude,
           id:$scope.locs[i].id, message:$scope.locs[i].name, current_owner:$scope.locs[i].current_owner,
            icon:$scope.getMarkerIcon($scope.locs[i].current_owner.id)});
      } catch (e) {
        $scope.markers.push({lat:$scope.locs[i].latitude, lng:$scope.locs[i].longitude,
           id:$scope.locs[i].id, message:$scope.locs[i].name, current_owner:$scope.locs[i].current_owner,
            icon:$scope.getMarkerIcon(null)});
      }
    }

    for (var i = 0; i < $scope.locs.length; i++) {
      $scope.paths.push({latlngs:[$scope.locs[i].latitude, $scope.locs[i].longitude], radius:25, type:"circle", color:"green", opacity:0.5});
    }

    $scope.captureSelected = function(){
      $scope.working=true;
        document.getElementById('captcha').style.display = "block";
        all_ready = function(token) {
            GameStateService.captureLocation($scope.selectedMarker.lat, $scope.selectedMarker.lng, $scope.selectedMarker.id, token).then(function(data){
                if(data==='OK'){
                    toast.success("Piste vallattu joukkueellesi!");
                    GameStateService.getData().then(function(response){
                        $scope.locs = response.locs;
                        $scope.teams = response.teams;
                        for (var i = 0; i < $scope.locs.length; i++) {
                            if($scope.locs[i].id == $scope.selectedMarker.id){
                                $scope.selectedMarker = $scope.locs[i];
                                $scope.selectedMarker.message = $scope.locs[i].name;
                            }
                        }
                    });
                }else{
                    toast.danger("Epävakaa GPS tai huijausyritys!");
                }
                document.getElementById('captcha').style.display = "none";
                $scope.working=false;
            });
        };
    }

    $scope.refreshGame = $interval(function(){
      $scope.newMarkers=[];
      for (var i = 0; i < $scope.locs.length; i++) {
        try {
          $scope.newMarkers.push({lat:$scope.locs[i].latitude, lng:$scope.locs[i].longitude,
             id:$scope.locs[i].id, message:$scope.locs[i].name, current_owner:$scope.locs[i].current_owner,
              icon:$scope.getMarkerIcon($scope.locs[i].current_owner.id)});
        } catch (e) {
          $scope.newMarkers.push({lat:$scope.locs[i].latitude, lng:$scope.locs[i].longitude, id:$scope.locs[i].id,
             message:$scope.locs[i].name, current_owner:$scope.locs[i].current_owner, icon:$scope.getMarkerIcon(null)});
        }
      }
      if($scope.playerLoc){
        $scope.mergedMarkers = $scope.newMarkers.concat([{lat:$scope.playerLoc.lat, lng:$scope.playerLoc.lng, icon:$scope.playerIcon}])
      }else{
        $scope.mergedMarkers = $scope.newMarkers;
      }
    }, 1000);

    $scope.refreshGameState = $interval(function(){
      GameStateService.getData().then(function(response){
          $scope.locs = response.locs;
          $scope.teams = response.teams;
      })
    }, 10000);

    $scope.checkSelectedMarkerDistance = function(){
      $scope.distFromMarker = parseFloat(getDistanceFromLatLonInKm($scope.playerLoc.lat, $scope.playerLoc.lng, $scope.selectedMarker.lat, $scope.selectedMarker.lng)*1000).toFixed(1);
    }


    $scope.$on('leafletDirectiveMap.gamemap.drag', function(event, wrapper){
          $scope.cameraLock = false;
    });

    $scope.$on('leafletDirectiveMap.gamemap.click', function(event, wrapper){
          $scope.selectedMarker = null;
    });
    $scope.$on('leafletDirectiveMarker.gamemap.click', function(event, wrapper){
          for (var i = 0; i < $scope.mergedMarkers.length; i++) {
            if($scope.mergedMarkers[i].id==wrapper.model.id){
              $scope.selectedMarker=$scope.mergedMarkers[i];
            }
          }
          $scope.checkSelectedMarkerDistance();
    });

    $scope.showError = function(error){
      switch(error.code) {
          case error.PERMISSION_DENIED:
              $scope.locationError = "User denied the request for Geolocation.";
              break;
          case error.POSITION_UNAVAILABLE:
              $scope.locationError = "Location information is unavailable.";
              break;
          case error.TIMEOUT:
              $scope.locationError = "The request to get user location timed out.";
              break;
          case error.UNKNOWN_ERROR:
              $scope.locationError = "An unknown error occurred.";
              break;
          default:
              $scope.locationError = error.message;
              break;
      }
  }

}]);

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1);
  var a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c; // Distance in km
  return d;
}

var all_ready;
function captcha_done(token) {
    console.log(token);
    all_ready && all_ready(token);
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}
