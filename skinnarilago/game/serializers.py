from rest_framework import serializers, exceptions
from game.models import *

class TeamSerializer(serializers.ModelSerializer):
    points = serializers.SerializerMethodField()

    def get_points(self, obj):
        return obj.get_points()

    class Meta:
        model = Team
        fields = '__all__'

class LocationSerializer(serializers.ModelSerializer):
    current_owner = TeamSerializer()

    class Meta:
        model = Location
        fields = '__all__'
