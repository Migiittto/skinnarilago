from django.conf import settings # import the settings file
from game.models import *
import random
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

def game_context(request):
    if request.session.session_key is None:
        request.session.create()
    context={
        "DEBUG":settings.DEBUG,
    }

    return context
