# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

admin.site.register(Team)
admin.site.register(Location)
admin.site.register(Point)

# Register your models here.
