from __future__ import absolute_import

import os

from celery import Celery
from celery.schedules import crontab
from datetime import *
# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'skinnarilago.settings')

from django.conf import settings  # noqa

app = Celery('skinnarilago')
app.set_current()
# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
