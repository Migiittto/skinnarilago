from locust import HttpLocust, TaskSet, task

class UserBehavior(TaskSet):

    @task(1)
    def profile(self):
        self.client.get("/gamestate/")

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 100
    max_wait = 1000
